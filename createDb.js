conn = new Mongo();
db = conn.getDB("hospitalAnimal");


db.animal.insert([
    { _id: 1000, nombre: "mailo", edad: 3, color: "cafe", raza: "yorki", sexo:true }, 
    { _id: 1001, nombre: "maili", edad: 6, color: "cafe", raza: "yorki", sexo:false },
    { _id: 1002, nombre: "greis", edad: 5, color: "blanco y negro", raza: "dalmata", sexo:false }, 
    { _id: 1003, nombre: "coki", edad: 3, color: "veis", raza: "cooqui", sexo:true },
    { _id: 1004, nombre: "papu", edad: 2, color: "negro", raza: "chapi", sexo:true}, 
    { _id: 1005, nombre: "flash", edad: 5, color: "cafe", raza: "doberman", sexo:true},
    { _id: 1006, nombre: "chaca", edad: 5, color: "cafe", raza: "pitbull", sexo:false}, 
    { _id: 1007, nombre: "pupaley", edad: 8, color: "blanco", raza: "scoth", sexo:false},
]);

db.duenio.insert([
    {_id:10000, ci:"123456 Lp", phone:98765352,direccion:"Portada",nombre:"Benjamin",animal:[
        {animal:1000},
        {animal:1001},
        {animal:1002},
    ]},
    {_id:10001, ci:"1232346 Lp", phone:98723222,direccion:"kollasuyo",nombre:"Charly",animal:[
        {animal:1007}
    ]},
    {_id:10002, ci:"12343356 Lp", phone:21165352,direccion:"miraflores",nombre:"Samuel",animal:[
        {animal:1006}
    ]},
    {_id:10003, ci:"123453456 Lp", phone:122765352,direccion:"Munaypata",nombre:"Ximena",animal:[
        {animal:1005}
    ]},
    {_id:10004, ci:"112226 Lp", phone:987121212,direccion:"vino Tinto",nombre:"Nicole",animal:[
        {animal:1004},
        {animal:1003}
    ]},
]);

db.hospital.insert([
    {_id:50001, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10000}
    ], animal:[
        {animal:1000,
        internado: true},
    
    ]},
    {_id:50002, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10000}
    ], animal:[
        {animal:1001,
        internado:false},
    
    ]},
    {_id:50003, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10000}
    ], animal:[
        {animal:1002,internado:false},
    
    ]},
    {_id:50004, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10004}
    ],animal:[
        {animal:1003,internado:true}
    ]},
    {_id:50005, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10004}
    ],animal:[
        {animal:1003,internado:false}
    ]},
    {_id:50006, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10001}
    ],animal:[
        {animal:1007,internado:false}
    ]},
    {_id:50007, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10006}
    ],animal:[
        {animal:1003,internado:true}
    ]},
    {_id:50008, descripcion:"expediente de mascota por atropello",fecha_internacion: new Date(),fecha_alta:new Date(),duenio:[
        {duenio:10003}
    ],animal:[
        {animal:1005,internado:false}
    ]},
]);