# lista de requerimientos Db hospital de animales
1) listar a todos los animales registrados en el hospital
2) mostrar los animales internados
3) mostrar los dueños de los animales internados
4) mostrar los animales internados entre el año 2020 y 2021
6) desplega historial de los animales de color y raza

# init de mongoDB
    //para usuarios linux de distribusiones nueva viene el systemctl utilizar los soguientes comandos

    // iniciar mongod
        $sudo systemctl start mongod
    
    // verificar
        $sudo systemctl status mongod

    //  detener
        $sudo systemctl stop mongod

    // reiniciar
        $sudo systemctl restart mongod


# Mongo comandos
    //muestra las bases de datos que tenemos actualmente
        - show dbs;
        - show databases;

    // cambiar de db    
        - use [nombre de la db];
    
    //verificar en que db nos encontramos
        - db;

## Insertar datos
    //insertar collecciones
        fileInsert = la coleccion a insertarse
        - db.namecollection.insert(fileInsert);

    // insertar collecciones por manera de golpe
        -db.namecollection.insert([uno,dos,tres]);

## ¿Que archivos de colecciones tenemos en nustra DB?
    // mostrar los archivos de collecciones que tenemos en la db
        - show collections
## Desplegar collecciones
    //mostrar los contenidos de los archivos de colleciones
        - db.namecollection.find();

    // mostrar una colleccion del archivo de collecciones
        - db.namecollection.findOne()
## Condiciones 

        -db.namecollection.find({CondicionWhere});

        //los que tengan la _id = 1003
        -db.namecollection.find({_id: 1003});

## Actualizar documentos de una coleccion
    //metodo sencillo

    $ obtenemos el valor de la coleccion a editar
    - var obtener = db.namecollection.findOne({_id: 10030});

    (opcional)
    $mostramos si corre bien
    - obtener
    $mostramos atributo
    - obtener._id
    (end)

    $editamos valor [nota: solo actualizamos el objeto obtner y aun no en la db]
    - obtener._id= 103030

    $ actualizamos en la db
    - db.collectionname.save(obtener);

    // por update

    - var obtener = db.namecollection.findOne({_id: 10030});

    $asignamos un valor y un campo 
    - obtener.age =10;

    $ sintaxis de update
    - db.namecollection.update({condicionWhere}, Actualizacion)

    - db.namecollection.update({_id: 10030}, obtener);

    // por set solo aplica al primero que cumpla la condicion
    - db.namecollection.update({condicion},{$set :{que se aplicara}});
     - db.namecollection.update({age:10},{$set :{age:12}});

    $unset elimina el campo

    // para aplicar a los todos los que cumplan
    - db.namecollection.update({age:10},{$set :{age:12}},{multi:true});

## Delete

    // elimina a todos loq ue age = 10
    - db.namecollection.remove({age:10});

    // elimina coleccion
    - db.namecollection.drop()

    // eliminar db
    - db.dropDatabase()

### consultas avanzadas
#### operador 
 - $gt   >
 - $gte  >=
 - $lt   <
 - $lte  <=
 







